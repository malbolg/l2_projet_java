package ulco.cardGame.common.games.players;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CardPlayer extends BoardPlayer {

    private List<Component> cards;

    /**
     * Default inherited constructor
     * @param name
     */
    public CardPlayer(String name) {
        super(name);
        this.cards = new ArrayList<>();
    }

    @Override
    public Integer getScore() {
        return this.score;
    }

    @Override
    public Card play( Socket S )  {
        Card cardToPlay= null;

        try {
            String finalCardName= null;
            Component ans;
            do {
                ObjectOutputStream oos = new ObjectOutputStream(S.getOutputStream());
                oos.writeObject( "_PLAY" );

                oos= new ObjectOutputStream( S.getOutputStream() );
                oos.writeObject( this.cards );


                ObjectInputStream ios = new ObjectInputStream(S.getInputStream());
                ans= (Component)ios.readObject();

                for( Component C : cards ) {
                    if( C.getName().equals( ans.getName() ) ) {
                        cardToPlay= (Card)C;
                        break;
                    }
                }

                if( cardToPlay == null ) {
                    oos = new ObjectOutputStream(S.getOutputStream());
                    oos.writeObject( "_BADCHALLENGE" );
                }
            } while( cardToPlay == null );

        } catch ( Exception E ) {
            E.printStackTrace();
        }

        this.removeComponent( cardToPlay );
        return cardToPlay;
    }

    @Override
    public void addComponent(Component component) {
        cards.add((Card) component);
        component.setPlayer( this );
        // update current player score (cards in hand)
        this.score = cards.size();
    }

    @Override
    public void removeComponent(Component component) {

        // Remove card from hand
        cards.remove(component);

        // update current player score (cards in hand)
        this.score = cards.size();
    }

    @Override
    public List<Component> getComponents() {
        return this.cards;
    }

    @Override
    public List<Component> getSpecificComponents(Class classType) {
        // By default
        return new ArrayList<>(this.cards);
    }

    @Override
    public void shuffleHand() {
        // prepare to shuffle hand
        Collections.shuffle(cards);
    }

    @Override
    public void clearHand() {

        // by default clear player hand
        // unlink each card
        for (Component card : cards) {
            card.setPlayer(null);
        }

        this.cards = new ArrayList<>();
    }

    /**
     * Display some expected components of player
     *  - number of Cards
     */
    @Override
    public void displayHand() {

        System.out.println("------------- Your hand -------------");
        System.out.println("Cards: " + cards.size());
        System.out.println("-------------------------------------");
    }

    @Override
    public String toString() {
        return "CardPlayer{" +
                "name='" + name + '\'' +
                ", score=" + score +
                '}';
    }
}
